import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import {UserserviceService} from "../servicios/userservice.service"
import { Router } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  posts:any;
  user:any;
  isIn = false;
  constructor(private userlist:UserserviceService, private activatedRouted: ActivatedRoute, private router:Router) {
   
   }

  ngOnInit() {
    this.userlist.getPostAPI().subscribe(data => {
      console.log(data)
      this.posts = data.results;
    },
       error => {
         console.log("fallo el call de la API");
       
         console.log(error)
       });
  
      console.log(this.posts);
  }

  comment(id:number){
    this.userlist.setPostId(id);
    this.router.navigate(['/comments',id]);
  }

}
