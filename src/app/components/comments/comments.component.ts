
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import {UserserviceService} from "../servicios/userservice.service"
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  comment = {
    idUser: '',
    idPost: '',
    comment: ''
}
isLoaded=false;

  json: any;
  constructor(private userlist:UserserviceService, private activatedRouted: ActivatedRoute, private router:Router) { 
    this.activatedRouted.params.subscribe(params => {
      console.log("Parametro recibidio es: "+params['id'])
      // this.hero=this.herolist.getHero(params['id'])
      this.json=this.userlist.getCommentsAPI(params['id']).subscribe(data => {
        console.log(data)
        this.json = data;
        this.isLoaded = true;
        // console.log("cambio de estado isLoaded"+this.isLoaded);
          // this.isLoaded=true;
          // console.log("cambio de estado isLoaded"+this.isLoaded);
      },
         error => {
           console.log("fallo el call de la API");
         
           console.log(error)
         });
      // this.idnext = params['id']
    })
    
  }

  ngOnInit() {
  }
  guardar(forma:NgForm) {
    console.log("guardo los cambios");
    console.log("NgForm forma: "+ forma);
    console.log("forma value: "+ forma.value);
    console.log("creoHeroe "+ this.comment);
  this.comment.idUser = this.userlist.getUserId();
  this.comment.idPost = this.userlist.getPostId();
    this.userlist.postCommentAPI(this.comment).subscribe(data => {
      console.log(data)
      
    },
       error => {
         console.log("fallo el call de la API");
       
         console.log(error)
       });

       this.activatedRouted.params.subscribe(params => {
        console.log("Parametro recibidio es: "+params['id'])
        // this.hero=this.herolist.getHero(params['id'])
        this.json=this.userlist.getCommentsAPI(params['id']).subscribe(data => {
          console.log(data)
          this.json = data;
          this.isLoaded = true;
          // console.log("cambio de estado isLoaded"+this.isLoaded);
            // this.isLoaded=true;
            // console.log("cambio de estado isLoaded"+this.isLoaded);
        },
           error => {
             console.log("fallo el call de la API");
           
             console.log(error)
           });
        // this.idnext = params['id']
      })
  }

}
