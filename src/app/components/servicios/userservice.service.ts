import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/Rx';
import { Body } from '@angular/http/src/body';


@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  private users = [];
  private  posts = [];
  private  comments = [];
  private  userid: any;
  private  postid:any;
  private  user:any;
  private  post:any;
  // isLogged = false;
  constructor(private http:Http) { }

  getUsersAPI(){

    console.log("llamada a getUsers API");
  
     let header = new Headers({'Content-Type':'application/json'});
     let usersURL = "http://192.168.101.61:8080/social/users";
  
      return this.http.get(usersURL, {headers: header})
         .map(res =>{
           console.log(res.json());
           console.log("entro positivo REST getUsersAPI")
           this.users = res.json();
           return res.json();
         }, err => console.log("error: "+err.json()));
     }
  
     getPostAPI(){

      console.log("llamada a getPosts API");
    
       let header = new Headers({'Content-Type':'application/json'});
       let usersURL = "http://192.168.101.61:8080/social/posts";
    
        return this.http.get(usersURL, {headers: header})
           .map(res =>{
             console.log(res.json());
             console.log("entro positivo REST getUsersAPI")
             this.posts = res.json();
             return res.json();
           }, err => console.log("error: "+err.json()));
       }

       setUserId(id:number){
         this.userid = id;

       }

       setPostId(id:number){
         this.postid = id;
       }

       getUserArray(id:number){
         if(this.users!=undefined){
           for (let index = 0; index < this.users.length; index++) {
             if(id == this.users[index].id){
               this.user=this.users[index];
               console.log(this.user);
               
             }
             
           }
         }
       }

       getCommentsAPI(id:number){
        console.log("llama a getComments API");
    
        let header = new Headers({'Content-Type':'application/json'});
        let commentsURL = "http://192.168.101.61:8080/social/comment/"+id;
     
         return this.http.get(commentsURL, {headers: header})
            .map(res =>{
              console.log(res.json());
              console.log("entro positivo REST getCommentsAPI")
              this.comments = res.json().results;
              this.post = res.json().post;
              return res.json();
            }, err => console.log("error: "+err.json()));

       }

       postCommentAPI(form:any){

        console.log("llama a post Comment API");
      
         let header = new Headers({'Content-Type':'application/json'});
         let replyURL = "http://192.168.101.61:8080/social/comment";
         let body = form;
      
          return this.http.post(replyURL, body, {headers: header})
             .map(res =>{
               console.log(res.json());
               console.log("entro positivo REST getHeroesAPI")
              
               return res.json();
             }, err => console.log("error: "+err.json()));
         }

         getUserId(){
           return this.userid;
         }
         getPostId(){
          return this.postid;
        }


}
