import { Component, OnInit } from '@angular/core';
import {UserserviceService} from '../servicios/userservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users:any;
  constructor(private userlist:UserserviceService, private router:Router) { }

  ngOnInit() {

      
    this.userlist.getUsersAPI().subscribe(data => {
      console.log(data)
      this.users = data.results;
    },
       error => {
         console.log("fallo el call de la API");
       
         console.log(error)
       });
  
      console.log(this.users);
  }

  selectUser(id:number){
    this.router.navigate(['/post']);
    this.userlist.setUserId(id);

  }

}
