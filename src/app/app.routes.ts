import { RouterModule, Routes } from '@angular/router';
import {UsersComponent} from './components/users/users.component'
import { PostComponent } from './components/post/post.component';
import { CommentsComponent } from './components/comments/comments.component';





const routes: Routes = [
    { path: 'users', component: UsersComponent },
    { path: 'post', component: PostComponent},
    { path: 'comments/:id', component: CommentsComponent}, 
    { path: '**', pathMatch:'full', redirectTo: 'users' }
   
];

export const appRouting = RouterModule.forRoot(routes);